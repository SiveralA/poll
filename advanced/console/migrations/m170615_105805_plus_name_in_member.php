<?php

use yii\db\Migration;

class m170615_105805_plus_name_in_member extends Migration
{
    public function up()
    {
        $this->addColumn('poll_member', 'name', $this->string()->after('email'));
        
        return true;
    }

    public function down()
    {
        $this->dropColumn('poll_member', 'name');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
