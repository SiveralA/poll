<?php

use yii\db\Migration;

class m170601_070457_start_base extends Migration
{
    protected $tablePollInfo = "poll_info";
    
    protected $tableCategory = "poll_category";
    
    protected $tableMember = "poll_member";
    
    protected $tableQuestion = "poll_question";
    
    protected $tableAnswer = "poll_answer";
    
    protected $tableResultLine = "poll_result_line";
    
    
    
    public function up()
    {
        $this->createTable($this->tableCategory, [
            'category_id' => $this->primaryKey(),
            'category_name' => $this->string(64)->unique()->notNull()
        ]);
        
        $this->createTable($this->tableMember, [
            'member_id' => $this->primaryKey(),
            'email' => $this->string(64)->notNull(),
            'poll_link' => $this->string(255)->notNull(),
            'status' => $this->integer(),
            'poll_id' => $this->integer()->notNull()
        ]);
        
        $this->createTable($this->tableQuestion, [
            'question_id' => $this->primaryKey(),
            'poll_id' => $this->integer()->notNull(),
            'question' => $this->string(255)
        ]);
        
        $this->createTable($this->tableAnswer, [
            'answer_id' => $this->primaryKey(),
            'poll_id' => $this->integer()->notNull(),
            'question_id' => $this->integer()->notNull(),
            'answer_text' => $this->string(255),
            'correct' => $this->integer()
        ]);
        
        $this->createTable($this->tablePollInfo, [
            'poll_id' => $this->primaryKey(),
            'owner' => $this->integer()->notNull(),
            'date_create' => $this->dateTime(),
            'category_id' => $this->integer()->notNull(),
            'status' => $this->integer(),
            'description' => $this->string(255),
            'start_time' => $this->dateTime(),
            'finish_time' => $this->dateTime(),
            'visible' => $this->integer()
        ]);
        
        $this->createTable($this->tableResultLine, [
            'result_id' => $this->primaryKey(),
            'member_id' => $this->integer()->notNull(),
            'poll_id' => $this->integer()->notNull(),
            'question_id' => $this->integer()->notNull(),
            'answer_id' => $this->integer()->notNull(),
            'answer_time' => $this->dateTime(),
            'free_variant' => $this->string(255)
        ]);        
        
        //addForeignKeys
        
        $this->addForeignKey('fk_owner_to_userid', $this->tablePollInfo, 'owner', 'user', 'id');
        
        $this->addForeignKey('fk_to_category', $this->tablePollInfo, 'category_id', $this->tableCategory, 'category_id');
        
        $this->addForeignKey('fk_to_answer_id', $this->tableResultLine, 'answer_id', $this->tableAnswer, 'answer_id');
        
        $this->addForeignKey('fk_to_question_id', $this->tableResultLine, 'question_id', $this->tableQuestion, 'question_id');
        
        $this->addForeignKey('fk_to_member_id', $this->tableResultLine, 'member_id', $this->tableMember, 'member_id');
        
        $this->addForeignKey('fk_to_poll_id_fr_member', $this->tableMember, 'poll_id', $this->tablePollInfo, 'poll_id');
        
        $this->addForeignKey('fk_to_poll_id_fr_question', $this->tableQuestion, 'poll_id', $this->tablePollInfo, 'poll_id');
        
        $this->addForeignKey('fk_to_question_id_fr_answer', $this->tableAnswer, 'question_id', $this->tableQuestion, 'question_id');
        
        $this->addForeignKey('fk_to_poll_id_fr_answer', $this->tableAnswer, 'poll_id', $this->tablePollInfo, 'poll_id');
        
        $this->addForeignKey('fk_to_poll_id_fr_rl', $this->tableResultLine, 'poll_id', $this->tablePollInfo, 'poll_id');
        
        
        return true;
    }

    public function down()
    {
        $this->dropForeignKey('fk_owner_to_userid', $this->tablePollInfo);
        $this->dropForeignKey('fk_to_category', $this->tablePollInfo);
        $this->dropForeignKey('fk_to_poll_id_fr_member', $this->tableMember);
        $this->dropForeignKey('fk_to_poll_id_fr_question', $this->tableQuestion);
        $this->dropForeignKey('fk_to_question_id_fr_answer', $this->tableQuestion);
        $this->dropForeignKey('fk_to_poll_id_fr_answer', $this->tableAnswer);
        $this->dropForeignKey('fk_to_poll_id_fr_rl', $this->tableResultLine);
        $this->dropForeignKey('fk_to_answer_id', $this->tableResultLine);
        $this->dropForeignKey('fk_to_question_id', $this->tableResultLine);
        $this->dropForeignKey('fk_to_member_id', $this->tableResultLine);

        $this->dropTable($this->tablePollInfo);
        $this->dropTable($this->tableMember);
        $this->dropTable($this->tableQuestion);
        $this->dropTable($this->tableAnswer);
        $this->dropTable($this->tableResultLine);
        $this->dropTable($this->tableCategory);
        
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
