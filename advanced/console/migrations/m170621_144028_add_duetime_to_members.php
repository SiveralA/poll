<?php

use yii\db\Migration;

class m170621_144028_add_duetime_to_members extends Migration
{
    public function up()
    {
        $this->addColumn('poll_member', 'duetime', $this->dateTime()->after('status'));

        return TRUE;
    }

    public function down()
    {
        $this->dropColumn('poll_member', 'duetime');

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
