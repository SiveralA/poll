<?php

use yii\db\Migration;

class m170615_123343_plus_polllink_in_pollinfo extends Migration
{
    public function up()
    {
        $this->addColumn('poll_info', 'poll_link', $this->string(255)->notNull()->after('finish_time'));

        return TRUE;
    }

    public function down()
    {
        $this->dropColumn('poll_info', 'poll_link');

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
