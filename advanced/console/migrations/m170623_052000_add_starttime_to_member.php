<?php

use yii\db\Migration;

class m170623_052000_add_starttime_to_member extends Migration
{
    public function safeUp()
    {
        $this->addColumn('poll_member', 'starttime', $this->dateTime()->after('status'));

        return TRUE;
    }

    public function safeDown()
    {
        $this->dropColumn('poll_member', 'starttime');

        return TRUE;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170623_052000_add_starttime_to_member cannot be reverted.\n";

        return false;
    }
    */
}
