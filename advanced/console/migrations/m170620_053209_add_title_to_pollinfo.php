<?php

use yii\db\Migration;

class m170620_053209_add_title_to_pollinfo extends Migration
{
    public function up()
    {
        $this->addColumn('poll_info', 'title', $this->string(255)->after('status'));

        return TRUE;
    }

    public function down()
    {
        $this->dropColumn('poll_info', 'title');

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
