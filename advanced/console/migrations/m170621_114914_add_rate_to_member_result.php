<?php

use yii\db\Migration;

class m170621_114914_add_rate_to_member_result extends Migration
{
    public function up()
    {
        $this->addColumn('poll_member', 'rate', $this->string(8));

        return TRUE;
    }

    public function down()
    {
        $this->dropColumn('poll_member', 'rate');

        return TRUE;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
