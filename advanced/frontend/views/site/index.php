<?php

/* @var $this yii\web\View */

$this->title = 'Опросник';
?>
<div class="site-index">

<br><br><br>
    <div class="body-content">
<?php if (!Yii::$app->user->isGuest) { ?>
        <div class="row">
            <div class="col-lg-12">
                <h2>Главная</h2>
                <p><a class="btn btn-default" href="/poll/create">Создать опрос</a></p>
                <p><a class="btn btn-default" href="/poll">Просмотреть опросы</a></p>
            </div>
        </div>
<?php } else { ?>        
      <center>
          <h1><b>Poll Universe</b></h1>
          <h2><a href="site/signup">Регистрируйтесь</a></h2>
            <h2>Создавайте опросы</h2>
            <h2>Смотрите результаты</h2>
            <h2>Читайте книги</h2>
            <h2>Размножайтесь</h2>
            <h2>Путешевствуйте</h2>
            <h2>Ой... Не то...</h2>
            <h2></h2>
            <h2>Опросы. Бесплатно. Без SMS, но с <a href="site/signup">регистрацией</a>!</h2>
      </center>
        
<?php } ?>        
    </div>
</div>
