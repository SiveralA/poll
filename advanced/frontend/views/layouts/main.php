<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
            <!-- Animate CSS -->
        <link href="/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <!-- Material Design Icons -->
        <link href="/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

        <!-- Malihu Scrollbar -->
        <link href="/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <!-- Select 2 -->
        <link href="/vendors/bower_components/select2/dist/css/select2.css" rel="stylesheet">
        <!-- Bootstrap Date/Time picker -->
        <link href="/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

        <!-- Site CSS -->
        <link href="/css/bootstrap-table.css" rel="stylesheet">
        <link href="/css/app.css" rel="stylesheet">
        <!-- Page loader -->
<!--        <script src="/js/page-loader.min.js"></script>-->

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'PollUniverse',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'Про', 'url' => ['/site/about']],
        ['label' => 'Фидбек', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Зарегистрироваться', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выйти (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
<br><br><br><br><br>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        
    </div>
    
</div>

    <footer class="footer">
    <div class="container">
<!--        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>-->

<!--        <p class="pull-right"><?= Yii::powered() ?></p>-->
    </div>
</footer>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
        <!-- Simple Weather -->
        <script src="/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <!-- EasyPie Charts -->
        <script src="/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="/js/bootstrap-table.js"></script>

        <!-- Site Functions & Actions -->
        <script src="/js/app.min.js"></script>
        <!-- Moment -->
        <script src="/vendors/bower_components/moment/min/moment.min.js"></script>
        <!-- Bootstrap Date/Time Picker -->
        <script src="/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>        



    
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
