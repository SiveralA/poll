<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои опросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poll-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать новый опрос', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="card">
    <div class="card__header">
        <h2>Список моих созданных мной опросов</h2>
    </div>
<table data-toggle='table' data-show-refresh='true' data-show-toggle='true' data-show-columns='true' data-search='true' data-select-item-name='toolbar1' data-pagination='true' data-sort-name='date' data-sort-order='desc'>
<thead>
<tr>
<th data-field='title' data-sortable='true'>Название</th>
<th data-field='date'  data-sortable='true'>Дата создания</th>
<th data-field='process'  data-sortable='false'>Процесс</th>
<th data-field='actions'  data-sortable='false'>Действия</th> 
</tr>
</thead>
        <tbody>
            <?php
            foreach ($polls as $poll) {
                $done = $all = 0;
                foreach ($members as $member) {
                    if ($member->poll_id==$poll->poll_id) {
                        if ($member->status==1) $done++;
                        $all++;
                    }
                }
            $form = ActiveForm::begin();
                echo " 
                <tr>
                <input type='hidden' name='id' value=".$poll->poll_id.">
                    <td>".$poll->title."</td>
                    <td>".$poll->date_create."</td>
                    <td>".$done."/".$all."</td>
                    <td>"; ?>
               <?= Html::a('', ['view', 'id' => $poll->poll_id], ['class' => 'glyphicon glyphicon-eye-open']) ?> &nbsp;
               <?= Html::a('', ['total', 'id' => $poll->poll_id], ['class' => 'glyphicon glyphicon-search']) ?> &nbsp;
               <?= Html::a('', ['delete', 'id' => $poll->poll_id], ['class' => 'glyphicon glyphicon-trash', 'onclick' => 'confirm("Вы точно желаете Удалить этот опрос?")', 'data-method'=>'post']) ?> &nbsp;
            
<?php           echo "</td>";
echo            "</tr>";
            ActiveForm::end();
            }
            ?>
            
        </tbody>  
</table>
</div>

</div>
