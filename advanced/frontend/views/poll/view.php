<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Poll */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Polls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poll-view">
<div class="card widget-todo-lists">
    <div class="card__header card__header--highlight">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
</div>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="card widget-todo-lists">
                        <div class="card__header card__header--highlight">
                            <h2><?= $model->description; ?></h2>
                            <br>
                            <small>Категория: 
                                <?php foreach ($categories as $category) {
                                    if ($category->category_id == $model->category_id) echo "<b>".$category->category_name."</b>";
                                }   
                                ?>
                            </small> <br>
                            <small>Время старта доступности: <b><?= $model->start_time; ?></b></small> <br>
                            <small>Время конца  доступности: <b><?= $model->finish_time; ?></b></small> <br>
                            <small>Показ результатов пользователю: <b>
                            <?php 
                                if ($model->visible) echo "Показывать"; else echo "Не показывать";
                            ?>
                            </b></small> <br>
                        </div>

                        <div class="list-group">
<?php foreach ($questions as $question) {   ?>
                            <div class="list-group-item">
                                <div class="checkbox checkbox--char">
                                    <label>
                                        <span class="checkbox__helper"><i>Q</i></span>
                                        <span class="widget-todo-lists__info">
                                            <b><?= $question->question; ?></b>
                                        </span>
<?php foreach ($answers as $answer) {
if ($question->question_id == $answer->question_id) {
?>
                                        <span class="list-group__attrs">
                                            <span>
                                            <?php if ($answer->correct == 1) { ?>    
                                                <i class="zmdi zmdi-check-circle zmdi-hc-fw" title="Правильный вариант"></i>
                                            <?php } else { ?>
                                                <i class="zmdi zmdi-circle-o zmdi-hc-fw" title="Неправильный вариант"></i>
                                            <?php } ?>     
                                            </span>
                                            <span><?= $answer->answer_text; ?></span>
                                        </span>
<?php   } }   ?>  
                                    </label>
                                </div>
                            </div>
<?php   }    ?>  
                        </div>
    
                    </div>
    
<div class="card widget-todo-lists">
    <div class="card__header card__header--highlight">
        <h2>Участники опроса:</h2>
<div class="list-group list-group--striped">
<?php foreach ($members as $member) { ?>
                            <div class="list-group-item media">
                                <div class="pull-left">
                                    <div class="avatar-char"><i class="zmdi zmdi-email"></i></div>
                                </div>
                                <div class="media-body">
                                    <div class="list-group__heading"><?= $member->email; ?></div>
                                    <small class="list-group__text"><?= $member->name; ?></small>
                                </div>
                            </div>
<?php  }  ?>
</div>
    </div>
<input type='hidden' name='poll_id' class='form-control' value='<?= $model->poll_id ?>'>    
<input type='hidden' name='poll_link' class='form-control' value='<?= $model->poll_link ?>'>   
<?php if ($model->status == 0) { ?>
<section id="content">
    <div class="card">
        <div class="card__body">
            <h4>Добавление участников опроса <i class="zmdi zmdi-hospital zmdi-hc-fw" onclick='return add_new_member();'></i></h4>
            <div id="member_f_1" class='input-group'>
                <span class='input-group-addon'><i class='zmdi zmdi-pin-account zmdi-hc-fw'></i></span>
                <div class='col-sm-3'>
                    <div class='form-group'>
                        <input type='email' name='member[1][email]' class='form-control' placeholder='Email участника опроса'>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='form-group'>
                        <input type='text' name='member[1][name]' class='form-control' placeholder='Имя участника опроса'>
                    </div>
                </div>
                <span class="input-group-addon last" onclick='return add_new_member();'><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>
                <span class="input-group-addon last" onclick="$('#member_f_1').remove();"><i class="zmdi zmdi-minus zmdi-hc-fw"></i></span>
            </div>
<div id='plus_member'></div> 
<br><br>
    <?= Html::submitButton('Сохранить участников из списка', ['name' => 'addMembers','value' => 'addMembers','class' => 'btn btn--light']) ?>
        </div>
    </div>
</section>
    <div class="card">
        <div class="card__body">
            <h4>Добавление участников опроса файлом</h4>
    <?= $form->field($file, 'csvFile')->fileInput() ?>
    <?= Html::submitButton('Сохранить участников из файла', ['name' => 'addMembersPack','value' => 'addMemberssPack','class' => 'btn btn--light']) ?>
        </div>
    </div>
<?php } ?>
    
</div>
    
<div class="card widget-todo-lists">
    <div class="card__header card__header--highlight">
        <h2>Управление</h2>
    </div>
    <div class="list-group list-group--striped">
        <div class="list-group-item media">
            <?php  if ($model->status == 0) { ?>
            <center><?= Html::submitButton('Активировать опрос. Разослать приглашения участникам.', ['name' => 'activatePoll','value' => 'activatePoll','class' => 'btn btn-success']) ?></center>
            <?php } elseif ($model->status == 1) { ?>
            <center>Опрос активирован. </center>
            <?php } elseif ($model->status == 2) { ?>
            <center>
                <?= Html::a('Опрос закрыт. Просмотреть результаты', ['total', 'id' => $model->poll_id], ['class' => 'btn btn--light']) ?> &nbsp;
            </center>
            <?php } ?>
        </div>
    </div>
</div>
    
    
</div>

<?php ActiveForm::end(); ?>

<script>
var total_member = 1;
function add_new_member(){
    total_member++;
$('            <div id="member_f_'+total_member+'" class="input-group">\n\
                <span class="input-group-addon"><i class="zmdi zmdi-pin-account zmdi-hc-fw"></i></span>\n\
                <div class="col-sm-3">\n\
                    <div class="form-group">\n\
                        <input type="email" name="member['+total_member+'][email]" class="form-control" placeholder="Email участника опроса">\n\
                    </div>\n\
                </div>\n\
                <div class="col-sm-3">\n\
                    <div class="form-group">\n\
                        <input type="text" name="member['+total_member+'][name]" class="form-control" placeholder="Имя участника опроса">\n\
                    </div>\n\
                </div>\n\
                <span class="input-group-addon last" onclick=\'return add_new_member();\'><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>\n\
                <span class="input-group-addon last" onclick=\'$("#member_f_'+total_member+'").remove();\'><i class="zmdi zmdi-minus zmdi-hc-fw"></i></span>\n\
            </div>')
    .appendTo('#plus_member');
}    
</script>