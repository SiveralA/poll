<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\Poll;

$this->title = 'Данные статистики по опросу';
$this->params['breadcrumbs'][] = $this->title;
$member_counter = 0;
?>
<div class="poll-index">
<br>
 <div class="card">
    <div class="card__header">
        <h2>Данные статистики по опросу</h2>
    </div>
    <div class="card__body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Имя участника</th>
                    <th>E-Mail Участника</th>
                    <th>Дата/Время прохождения опроса</th>
                    <th>Время прохождения опроса</th>
                    <th>Результат прохождения</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($members as $member) {
                    $member_counter++;
                ?>
                <tr>
                    <td><?= $member_counter; ?></td>
                    <td><?= $member->name; ?></td>
                    <td><?= $member->email; ?></td>
                    <td><?= $member->duetime; ?></td>
                    <td><?= Poll::diffTimes($member->starttime,$member->duetime); ?></td>
                    <td><?= $member->rate; ?> %</td>
                </tr>
                <?php  }  ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>


