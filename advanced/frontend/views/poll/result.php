<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Спасибо. Тест пройден.';
?>
  
<?php $session = Yii::$app->session; ?>

<div class="poll-index">
    <br><br><br><br>
    <h1><?= Html::encode($this->title) ?></h1>
<?php
    foreach ($results as $result) {
        $an_arr[$result->question_id][$result->answer_id] = $result->answer_id;
    }
?>
<?php 
// Если разрешено показывать результаты прохождения теста пользователю
if ($poll_info['visible']==1) { ?>
<h2>Результаты теста:</h2>
    <div class="list-group">
        <?php foreach ($questions as $question) {   ?>
            <?php $result_c[$question->question_id]['correct'] = $result_c[$question->question_id]['uncorrect'] = 0; ?>
        <div class="list-group-item">
                    <div class="checkbox checkbox--char">
                        <label>
                            <span class="checkbox__helper"><i>Q</i></span>
                            <span class="widget-todo-lists__info">
                                <b><?= $question->question; ?></b>
                            </span>
                            <?php
                            foreach ($answers as $answer) {
                                if ($question->question_id == $answer->question_id) {
                                    ?>
                                    <span class="list-group__attrs">
                                        <span>
                                            <?php
                                            if (($answer->correct == 1) && isset($an_arr[$answer->question_id][$answer->answer_id])) {
                                                echo "<i class='zmdi zmdi-check-circle zmdi-hc-fw' title='Правильный ответ'></i>";
                                                $result_c[$question->question_id]['correct'] ++;
                                            } elseif (($answer->correct == 0) && isset($an_arr[$answer->question_id][$answer->answer_id])) {
                                                echo "<i class='zmdi zmdi-minus-circle-outline zmdi-hc-fw' title='Неправильный ответ'></i>";
                                                $result_c[$question->question_id]['uncorrect'] ++;
                                            } elseif ($answer->correct == 1) {
                                                echo "<i class='zmdi zmdi-minus zmdi-hc-fw' title='Не отвеченный ответ'></i>";
                                                $result_c[$question->question_id]['uncorrect'] ++;
                                            } else {
                                                echo "<i class='zmdi zmdi-circle-o zmdi-hc-fw'></i>";
                                            }
                                            ?>
                                        </span>
                                        <span><?= $answer->answer_text; ?></span>
                                    </span>
            <?php } } ?>
                        </label>
                    </div>
                </div>
        <?php   }    ?>
        <?php
// Расчёт правильных и неправильных ответов пользователя. Вычисление процента прохождения теста.
        $count_quest = count($result_c);
        $persent = 0;
        foreach ($result_c as $key => $quest) {
            if ($quest['uncorrect']!=0) $result_c[$key] = 0; else $result_c[$key] = 1;
        }
        foreach ($result_c as $value) {
            $persent = $persent + $value;
        }
        $persent = round($persent / $count_quest * 100 , 2);
        ?>
        <h2>Процент прохождения теста = <?= $persent; ?>%</h2>
    </div>
</div>
<?php 
frontend\controllers\PollController::closeMemberStatus ($persent); // Отметка прохождения теста поользователем (по приглашению) и сохранения % результата
        } else {
// Если нельзя показывать результаты - просто расчёт процента прохождения
foreach ($questions as $question) {
    $result_c[$question->question_id]['correct'] = $result_c[$question->question_id]['uncorrect'] = 0;
    foreach ($answers as $answer) {
        if ($question->question_id == $answer->question_id) {
            if (($answer->correct == 1) && isset($an_arr[$answer->question_id][$answer->answer_id])) {
                $result_c[$question->question_id]['correct']++;
            } elseif (($answer->correct == 0) && isset($an_arr[$answer->question_id][$answer->answer_id])) {
                $result_c[$question->question_id]['uncorrect']++;
            } elseif ($answer->correct == 1) {
                $result_c[$question->question_id]['uncorrect']++;
            }
        }
    }
}
$count_quest = count($result_c);
$persent = 0;
foreach ($result_c as $key => $quest) {
    if ($quest['uncorrect'] != 0)
        $result_c[$key] = 0;
    else
        $result_c[$key] = 1;
}
foreach ($result_c as $value) {
    $persent = $persent + $value;
}
$persent = round($persent / $count_quest * 100, 2);

frontend\controllers\PollController::closeMemberStatus ($persent); // Отметка прохождения теста поользователем (по приглашению) и сохранения % результата
    echo "<h3>Спасибо за прохождение теста. Результаты отправлены автору.</h3>";
}
?>
