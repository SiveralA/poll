<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Poll */

$this->title = 'Прохождение теста';
?>
<div class="poll-do">
    <?php $form = ActiveForm::begin(); ?>
<br><br><br>
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<input type="hidden" name="poll_id" value="<?= $poll['poll_id']; ?>">
<input type="hidden" name="member_id" value="<?= $member->member_id; ?>">
<input type="hidden" name="starttime" value="<?= date("Y-m-d h:i:s") ?>">

<div class="card">
    <div class="card__header">
        <h2>Здравствуйте,
            <?php if ($member->name!='') echo $member->name; else $member->email; ?>
        </h2>
        <br>
        <h2>Опрос <small><?php $poll['description']; ?></small></h2>
    </div>
    <div class="card__body">
        <div class="tab-wizard">

            <ul class="tab-nav tab-nav--centered tab-wizard__nav">
                <?php
                $i_title = 0;
                foreach ($questions as $question) {
                    $i_title++;
                ?>
                <li<?php if ($i_title==1) { ?> class="active" <?php } ?> ><a href="#tab-wizard-<?= $i_title ?>" data-toggle="tab"><?= $i_title ?></a>
                    <!-- Доделать оповещениее о прохождении этого вопроса <i class="zmdi zmdi-check-square zmdi-hc-fw" type="hidden"></i> -->
                </li>
                <?php } ?>

            </ul>
            <div class="tab-content">
                <?php
                $i_question = $i_answer = 0;
                foreach ($questions as $question) {
                    $i_question++;
                ?>
                <div class="tab-pane fade <?php if ($i_question==1) { ?> active in <?php } ?>" id="tab-wizard-<?= $i_question ?>">
                    <p><h3><?= $question->question ?></h3></p>
                    <?php
                    $count_correct = 0;
                    foreach ($answers as $answer) {
                        if ($answer->correct == 1 && $answer->question_id == $question->question_id) $count_correct++;
                    }
                    ?>
                    <?php
                    if ($count_correct==1) echo "(Есть только 1 вариант ответа)"; else echo "(Есть несколько вариант ответа)";
                    ?>

                    <?php foreach ($answers as $answer) {
                        $i_answer++;
                        if ($question->question_id == $answer->question_id) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="question[<?= $question->question_id; ?>][<?= $i_answer; ?>]" value="<?= $answer->answer_id; ?>">
                                    <i class="input-helper"></i>
                                    <?= $answer->answer_text; ?>
                                </label>
                            </div>
                        <?php   } }   ?>
                </div>
                <?php } ?>

                <ul class="pagination pagination--alt">
                    <li class="tab-wizard__first tab-wizard__previous"><span><i class="zmdi zmdi-more-horiz"></i></span></li>
                    <li class="tab-wizard__previous"><span><i class="zmdi zmdi-long-arrow-left"></i></span></li>
                    <li class="tab-wizard__next"><span><i class="zmdi zmdi-long-arrow-right"></i></span></li>
                    <li class="tab-wizard__last tab-wizard__next"><span><i class="zmdi zmdi-more-horiz"></i></span></li>
                </ul>
            </div>

        </div>
    </div>
</div>

<?= Html::submitButton('Подтвердить прохождение опроса', ['name' => 'acceptDonePoll','value' => 'acceptDonePoll','class' => 'btn btn-success']) ?>

<?php ActiveForm::end(); ?>