<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Poll */

$this->title = 'Создать тест';
$this->params['breadcrumbs'][] = ['label' => 'Polls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poll-create">

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories
    ]) ?>

</div>
