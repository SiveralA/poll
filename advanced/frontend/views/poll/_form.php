<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\Poll */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poll-form">

    <?php $form = ActiveForm::begin(); ?>

<section id="content">
    <div class="card">
        <div class="card__header">
            <h2>Создание теста <small>Введите данные вопросов, ответов и настройки теста</small></h2>
        </div>
        <div class="card__body">
            <input type='text' name='poll_title' class='form-control input-lg' placeholder='Введите название теста' required autofocus>
            <input type='text' name='poll_description' class='form-control input-lg' placeholder='Введите описание теста' required>
                <?php
                    $categories = frontend\models\Category::find()->all();
                    $items = ArrayHelper::map($categories,'category_id','category_name');
                    $params = ['prompt' => 'Выберите категорию'];
                    echo $form->field($model, 'category')->dropDownList($items,$params)->label('');
                 
                ?>
<!--            
                    <select class="select2 form-control" name="category" required>
                        <option value=''>Выберите категорию</option>
                        <?php
                        /*foreach ($categories as $category) { 
                            echo "<option value='".$category->category_id."'>".$category->category_name."</option>";
                        }   */ 
                        ?>
                    </select>-->
           
                        <!--    <div class="form-group">
                                    <label>Date Time</label>
                                    <input type="text" class="form-control input-mask" data-mask="00/00/0000 00:00:00" placeholder="eg: 00/00/0000 00:00:00">
                                    <i class="form-group__bar"></i>
                                </div>-->

            <input type='hidden' name='status' class='form-control input-lg' value="0">
            <input type='datetime-local' name='start_time' class='form-control input-lg' required>
            <p>Начало доступности прохождения теста</p>
            <input type='datetime-local' name='finish_time' class='form-control input-lg' required>
            <p>Конец доступности прохождения теста</p>
<!--            <input type='text' name='visible' class='form-control input-lg' required value="1">-->
                    <select class="select2 form-control" name='visible' required>
                        <option value=''>Выберите видимость результатов пользователями</option>
                        <option value='0'>Не показывать результаты после теста</option>
                        <option value='1'>Показывать результаты после теста</option>
                    </select>
        </div>
    </div>
</section>
            
            
<section id="content">
    <div class="card">
        <div class="card__body">
            <h4>Вопрос № 1</h4>
            <div class='input-group input-group-lg'>
                <span class='input-group-addon'><i class='zmdi zmdi-help-outline zmdi-hc-fw'></i></span>
                <div class='form-group'>
                    <input type='text' name='question[1][name]' class='form-control input-lg' placeholder='Введите вопрос'>
                    <i class='form-group__bar'></i>
                </div>
                <span class="input-group-addon last" onclick='return add_new_answer(1);'><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>
            </div>
            <br>
            <div id="answer_f_1" class='input-group'>
                <span class='input-group-addon'><i class='zmdi zmdi-alert-circle-o zmdi-hc-fw'></i></span>
                <div class='form-group'>
                    <input type='text' name='question[1][answer][1][name]' class='form-control' placeholder='Введите вариант ответа'>
                     <i class='form-group__bar'></i>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="hidden" value="0" name="question[1][answer][1][correct]">
                        <input type="checkbox" value="1" name="question[1][answer][1][correct]" title="Этот вариант правильный">
                        <i class="input-helper"></i>
                    </label>
                </div>
                <span class="input-group-addon last" onclick='return add_new_answer(1);'><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>
                <span class="input-group-addon last" onclick="$('#answer_f_1').remove();"><i class="zmdi zmdi-minus zmdi-hc-fw"></i></span>
            </div>
<div id='plus_answer_1'></div> 
        </div>
    </div>

</section>
            
<div id='plus_question'></div> 

<div id='raz'>
    <input type="button" class="btn btn-default btn-success btn--icon-text" value=" + Добавить еще вопрос">
    <br>
</div> 
<br><br><br>

                                                                                                                                                

<script>
var question_id = 1;
document.querySelector('#raz input').onclick = function() {
  question_id++;
  var theDiv = document.createElement('div');  // создать новый тег div
  theDiv.innerHTML = '<section id="content">\n\
    <div class="card">\n\
        <div class="card__body">\n\
            <h4>Вопрос № '+question_id+'</h4>\n\
            <div class="input-group input-group-lg">\n\
                <span class="input-group-addon"><i class="zmdi zmdi-help-outline zmdi-hc-fw"></i></span>\n\
                <div class="form-group">\n\
                    <input type="text" name="question['+question_id+'][name]" class="form-control input-lg" placeholder="Введите вопрос">\n\
                    <i class="form-group__bar"></i>\n\
                </div>\n\
                <span class="input-group-addon last" onclick="return add_new_answer();"><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>\n\
            </div>\n\
            <br>\n\
            <div id="answer_f_'+ total_answer + '" class="input-group">\n\
                <span class="input-group-addon"><i class="zmdi zmdi-alert-circle-o zmdi-hc-fw"></i></span>\n\
                <div class="form-group">\n\
                    <input type="text" name="question['+question_id+'][answer]['+total_answer+'][name]" class="form-control" placeholder="Введите вариант ответа">\n\
                    <i class="form-group__bar"></i>\n\
                </div>\n\
                 <div class="checkbox">\n\
                    <label>\n\
                        <input type="hidden" value="0" name="question['+question_id+'][answer]['+ total_answer + '][correct]">\n\
                        <input type="checkbox" value="1" name="question['+question_id+'][answer]['+ total_answer + '][correct]" title="Этот вариант правильный">\n\
                        <i class="input-helper"></i>\n\
                    </label>\n\
                </div>\n\
                <span class="input-group-addon last" onclick=\'return add_new_answer('+question_id+');\'><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>\n\
                <span class="input-group-addon last" onclick=\'$("#answer_f_'+ total_answer + '").remove();\'><i class="zmdi zmdi-minus zmdi-hc-fw"></i></span>\n\
            </div>\n\
<div id="plus_answer_'+question_id+'"></div> \n\
        </div>\n\
    </div>\n\
</section>';  // его содержимое
  this.parentNode.appendChild(theDiv);  // поместить новый тег последним в div с id="raz" (вместо this.parentNode может быть любой другой элемент DOM)
  getComputedStyle(theDiv).opacity;
  theDiv.style.opacity = '1';
}
</script>

<script>
var total_answer = 1;
function add_new_answer(num_question){
    total_answer++;
$('<div id="answer_f_'+ total_answer + '" class="input-group">\n\
    <span class="input-group-addon"><i class="zmdi zmdi-alert-circle-o zmdi-hc-fw"></i></span>\n\
     <div class="form-group">\n\
         <input type="text" name="question['+question_id+'][answer]['+total_answer+'][name]" class="form-control" placeholder="Введите вариант ответа">\n\
         <i class="form-group__bar"></i>\n\
     </div>\n\
      <div class="checkbox">\n\
        <label>\n\
            <input type="hidden" value="0" name="question['+question_id+'][answer]['+ total_answer + '][correct]">\n\
            <input type="checkbox" value="1" name="question['+question_id+'][answer]['+ total_answer + '][correct]" title="Этот вариант правильный">\n\
            <i class="input-helper"></i>\n\
        </label>\n\
    </div>\n\
     <span class="input-group-addon last" onclick=\'return add_new_answer('+num_question+');\'><i class="zmdi zmdi-hospital zmdi-hc-fw"></i></span>\n\
     <span class="input-group-addon last" onclick=\'$("#answer_f_'+ total_answer + '").remove();\'><i class="zmdi zmdi-minus zmdi-hc-fw"></i></span>\
 </div>')
    .appendTo('#plus_answer_'+num_question+'');
}    
</script>

    <div class="form-group">
        <?= Html::submitButton('Сохранить опрос', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
