<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poll_question".
 *
 * @property integer $question_id
 * @property integer $poll_id
 * @property string $question
 *
 * @property PollAnswer[] $pollAnswers
 * @property PollInfo $poll
 * @property PollResultLine[] $pollResultLines
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poll_id'], 'required'],
            [['poll_id'], 'integer'],
            [['question'], 'string', 'max' => 255],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'poll_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'question_id' => 'Question ID',
            'poll_id' => 'Poll ID',
            'question' => 'Question',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollAnswers()
    {
        return $this->hasMany(PollAnswer::className(), ['question_id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(PollInfo::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollResultLines()
    {
        return $this->hasMany(PollResultLine::className(), ['question_id' => 'question_id']);
    }
}
