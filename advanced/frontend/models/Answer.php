<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poll_answer".
 *
 * @property integer $answer_id
 * @property integer $poll_id
 * @property integer $question_id
 * @property string $answer_text
 * @property integer $correct
 *
 * @property PollInfo $poll
 * @property PollQuestion $question
 * @property PollResultLine[] $pollResultLines
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poll_id', 'question_id'], 'required'],
            [['poll_id', 'question_id', 'correct'], 'integer'],
            [['answer_text'], 'string', 'max' => 255],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'poll_id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'question_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'answer_id' => 'Answer ID',
            'poll_id' => 'Poll ID',
            'question_id' => 'Question ID',
            'answer_text' => 'Answer Text',
            'correct' => 'Correct',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(PollInfo::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(PollQuestion::className(), ['question_id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollResultLines()
    {
        return $this->hasMany(PollResultLine::className(), ['answer_id' => 'answer_id']);
    }
}
