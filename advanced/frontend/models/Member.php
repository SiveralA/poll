<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poll_member".
 *
 * @property integer $member_id
 * @property string $email
 * @property string $poll_link
 * @property integer $status
 * @property integer $poll_id
 *
 * @property PollInfo $poll
 * @property PollResultLine[] $pollResultLines
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'poll_link', 'poll_id'], 'required'],
            [['status', 'poll_id'], 'integer'],
            [['email'], 'string', 'max' => 64],
            ['email', 'email'],
            [['poll_link','name'], 'string', 'max' => 128],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'poll_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_id' => 'Member ID',
            'email' => 'Email',
            'name' => 'Name',
            'poll_link' => 'Poll Link',
            'status' => 'Status',
            'poll_id' => 'Poll ID',
            'duetime' => 'Due Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(PollInfo::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollResultLines()
    {
        return $this->hasMany(PollResultLine::className(), ['member_id' => 'member_id']);
    }
}
