<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poll_result_line".
 *
 * @property integer $result_id
 * @property integer $member_id
 * @property integer $poll_id
 * @property integer $question_id
 * @property integer $answer_id
 * @property string $answer_time
 * @property string $free_variant
 *
 * @property Answer $answer
 * @property Member $member
 * @property Info $poll
 * @property Question $question
 */
class Result extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_result_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'poll_id', 'question_id', 'answer_id'], 'required'],
            [['member_id', 'poll_id', 'question_id', 'answer_id'], 'integer'],
            [['answer_time'], 'safe'],
            [['free_variant'], 'string', 'max' => 255],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'answer_id']],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['member_id' => 'member_id']],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'poll_id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'question_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'result_id' => 'Result ID',
            'member_id' => 'Member ID',
            'poll_id' => 'Poll ID',
            'question_id' => 'Question ID',
            'answer_id' => 'Answer ID',
            'answer_time' => 'Answer Time',
            'free_variant' => 'Free Variant',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['answer_id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['member_id' => 'member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Info::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['question_id' => 'question_id']);
    }
}
