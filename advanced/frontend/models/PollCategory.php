<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poll_category".
 *
 * @property integer $category_id
 * @property string $category_name
 *
 * @property PollInfo[] $pollInfos
 */
class PollCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'required'],
            [['category_name'], 'string', 'max' => 64],
            [['category_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollInfos()
    {
        return $this->hasMany(PollInfo::className(), ['category_id' => 'category_id']);
    }
}
