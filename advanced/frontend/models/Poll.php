<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "poll_info".
 *
 * @property integer $poll_id
 * @property integer $owner
 * @property string $date_create
 * @property integer $category_id
 * @property integer $status
 * @property string $description
 * @property string $start_time
 * @property string $finish_time
 * @property integer $visible
 *
 * @property PollAnswer[] $pollAnswers
 * @property User $owner0
 * @property PollCategory $category
 * @property PollMember[] $pollMembers
 * @property PollQuestion[] $pollQuestions
 * @property PollResultLine[] $pollResultLines
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner', 'category_id'], 'required'],
            [['owner', 'category_id', 'status', 'visible'], 'integer'],
            [['date_create', 'start_time', 'finish_time'], 'safe'],
            [['description','poll_link','title'], 'string', 'max' => 255],
            [['owner'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PollCategory::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'poll_id' => 'Poll ID',
            'owner' => 'Owner',
            'date_create' => 'Date Create',
            'category_id' => 'Category ID',
            'status' => 'Status',
            'title' => 'Title',
            'description' => 'Description',
            'start_time' => 'Start Time',
            'finish_time' => 'Finish Time',
            'poll_link' => 'Poll Link',
            'visible' => 'Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollAnswers()
    {
        return $this->hasMany(PollAnswer::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PollCategory::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollMembers()
    {
        return $this->hasMany(PollMember::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollQuestions()
    {
        return $this->hasMany(PollQuestion::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollResultLines()
    {
        return $this->hasMany(PollResultLine::className(), ['poll_id' => 'poll_id']);
    }
    
    public static function diffTimes ($startTime,$endTime) {
        return date("H:i:s", mktime(0, 0, strtotime($endTime) - strtotime($startTime)));
    }
}
