<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


use frontend\models\Poll;
use frontend\models\PollSearch;
use frontend\models\Question;
use frontend\models\Answer;
use frontend\models\Member;
use frontend\models\Category;
use frontend\models\Result;
use frontend\models\File;


/**
 * PollController implements the CRUD actions for Poll model.
 */
class PollController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['do', 'result', 'polldone'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create' , 'index', 'do', 'result', 'polldone', 'view', 'delete', 'total'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Poll models.
     * @return mixed
     */
    /*
     * Вывод данных для формирования списка созданных пользователем опросов
     */
    public function actionIndex()
    {
        $polls = Poll::find()->where(['owner' => Yii::$app->user->identity->id])->all();
        $members = Member::find()->all();
        
        return $this->render('index', [
            'polls' => $polls,
            'members' => $members
        ]);
    }

    /**
     * Displays a single Poll model.
     * @param integer $id
     * @return mixed
     */
    /*
     * Просмотр дозданного опроса
     * Возможности:
     *   Просмотр информации о созданном опросе   
     *      - добавление пользователя теста
     *      - пакетное добавление пользователей (через csv-файл)
     *      - активация теста:
     *          - Изменение статуса теста на "Активен"
     *          - Отработка рассылки всех подвязанных e-mail адресов
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $poll_id = Yii::$app->request->post()['poll_id'];
// Добавление пользователей из строк сгенерированных JS на view:
            if (Yii::$app->request->post('addMembers')) {
                $member_array = Yii::$app->request->post();
                $poll_id = $member_array ['poll_id'];
                $poll_link = $member_array ['poll_link'];
                unset ($member_array['poll_id'],$member_array['_csrf-frontend'],$member_array ['poll_link']);
                $member_count = count($member_array['member']);
                for ($member_num=1;$member_num<=$member_count;$member_num++) {
// Ищем, нет ли повторения email значения в базе в таком опросе:
                    if (Member::find()->where(['poll_id' => $poll_id,'email' => $member_array['member'][$member_num]['email']])->all()) {
                    } else {
                    $member = new Member([
                        'email' => $member_array['member'][$member_num]['email'],
                        'name' => $member_array['member'][$member_num]['name'],
                        'poll_link' => $poll_link,
                        'status' => 0,
                        'poll_id' => $poll_id
                    ]);
                    $member->save();
                }
                }
            }
// Пакетная загрузка в базу через загрузку csv файла с полями: email & name
            if (Yii::$app->request->post('addMembersPack')) {
                $upfile = new File();

                
                if (Yii::$app->request->isPost) {
                    $upfile->csvFile = UploadedFile::getInstance($upfile, 'csvFile');
// Сохранение файла в файловую систему -> чтение его содержимого -> сохранение данных в базу (если нет повторений) -> удаление файла
                    if ($upfile->upload()) {
                        $member_array = Yii::$app->request->post();
                        $poll_id = $member_array ['poll_id'];
                        $poll_link = $member_array ['poll_link'];
                        $filelink = 'uploads/'.$upfile->csvFile->name;
                        $row = 0;
                        if (($handle = fopen($filelink,"r+")) !== FALSE) {
                        while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
                            if (!Member::find()->where(['poll_id' => $poll_id,'email' => $data[0]])->all()) {
                                $member = new Member([
                                    'email' => $data[0],
                                    'name' => $data[1],
                                    'poll_link' => $poll_link,
                                    'status' => 0,
                                    'poll_id' => $poll_id
                                ]);
                                if ($member->validate()) $member->save();
                            }
                        $row++;
                        }
                        fclose($handle);
                        } 
                        unlink($filelink);
                    }
                }
            }
// Активация опроса (изменение статуса)            
            if (Yii::$app->request->post('activatePoll')) {
                    Poll::updateAll(['status' => 1], ['=', 'poll_id', $poll_id]);
                    Yii::$app->session->setFlash('success', 'Опрос активирован');
// Рассылка привязанным к опросу пользователям приглашений с ссылкой на прохождение теста
                $members = Member::find()->where(['poll_id' => $poll_id])->all();
                foreach ($members as $member) {
                    $link = md5($member->email.$member->name.$member->poll_link.$member->poll_id);
                    if ($member->name == '') $mail_name = $member->email; else $mail_name = $member->name;
                    Yii::$app->mailer->compose()
                        ->setFrom('admin@poll.com')
                        ->setTo($member->email)
                        ->setSubject('Poll приглашает Вас '.$mail_name.', учавствовать в опросе' )
                        ->setHtmlBody('<p><b>Тупо надо</b></p>'
                            . '<p>'.$mail_name.', перейдите по ссылке - <a href="http://spalah.poll/poll/do?token='.$link.'">Перейти для прохождения теста</a> </p>')
                    ->send();
                }
            }
// Возвращение на страницу view с обновившимися данными            
            $file = new File();
            $questions  = Question::find()->where(['poll_id' => $poll_id])->all();
            $answers    = Answer::find()->where(['poll_id' => $poll_id])->all();
            $categories = Category::find()->all();
            $members    = Member::find()->where(['poll_id' => $poll_id])->all();
            return $this->render('view', [
                'model'      => $model,
                'questions'  => $questions,
                'answers'    => $answers,
                'categories' => $categories,
                'members'    => $members,
                'file'       => $file,
            ]);
        } else {
// Если не было POSTa - просто показыавть view            
            $file = new File();
            $questions  = Question::find()->where(['poll_id' => $model->poll_id])->all();
            $answers    = Answer::find()->where(['poll_id' => $model->poll_id])->all();
            $categories = Category::find()->all();
            $members    = Member::find()->where(['poll_id' => $model->poll_id])->all();
            return $this->render('view', [
                'model'      => $model,
                'questions'  => $questions,
                'answers'    => $answers,
                'categories' => $categories,
                'members'    => $members,
                'file'       => $file,
            ]);
        }
    }

    /**
     * Creates a new Poll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
     * Принятие сформированных данных от view CREATE.
     * Разборка массива данных и сохранение.
     * 
     */
    public function actionCreate()
    {
        $model = new Poll();
        $transaction = Yii::$app->db->beginTransaction();

        if ($model->load(Yii::$app->request->post())) {
            $poll_array = Yii::$app->request->post();
            
            $poll_owner = Yii::$app->user->identity->id; 
            $poll_title = $poll_array['poll_title'];
            $poll_description = $poll_array['poll_description'];
            $poll_category = $poll_array['Poll']['category'];
            $poll_status = $poll_array['status'];
            $poll_start_time  = str_replace("T", " ", $poll_array['start_time']).":00";
            $poll_finish_time = str_replace("T", " ", $poll_array['finish_time']).":00";
            $poll_link = \Yii::$app->getSecurity()->generateRandomString(64);
            $poll_visible = $poll_array['visible'];
            
            unset ($poll_array['poll_title']);
            unset ($poll_array['poll_description']);
            unset ($poll_array['Poll']);
            unset ($poll_array['status']);
            unset ($poll_array['start_time']);
            unset ($poll_array['finish_time']);
            unset ($poll_array['visible']);
            unset ($poll_array['_csrf-frontend']);

            $question_count = count($poll_array['question']);

                $poll_info = new Poll([
                    "owner" => $poll_owner,
                    "date_create" => date ("Y-m-d h:i:s"),
                    "status" => $poll_status,
                    "title" => $poll_title,
                    "category_id" => $poll_category,
                    "description" => $poll_description,
                    "start_time" => $poll_start_time,
                    "finish_time" => $poll_finish_time,
                    "poll_link" => $poll_link,
                    "visible" => $poll_visible
                ]);

                if ($poll_info->save()) {
                    $transaction_result_poll = 1;
                } else {
                    $transaction_result_poll = 0;
                }
                $poll_id = $poll_info->poll_id;

            for ($question_num=1;$question_num<=$question_count;$question_num++) {
                $question = $poll_array['question'][$question_num];
                $question_name = $question['name'];
                unset ($question['name']);
                
               $question_save = new Question([
                   "poll_id" => $poll_id,
                   "question" => $question_name
               ]);

                if ($question_save->save()) { 
                    $transaction_result_question = 1;
                } else {
                    $transaction_result_question = 0;
                }
// Получение question_id путём опознания последнего сохранённого id опроса
               $question_id = Yii::$app->db->getLastInsertID();
                
                foreach ($question as $answers) {
                    foreach ($answers as $answer) {
                        $answer_name = $answer['name'];
                        $answer_correct = $answer['correct'];

                        $answer_save = new Answer([
                            "poll_id" => $poll_id,
                            "question_id" => $question_id,
                            "answer_text" => $answer_name,
                            "correct" => $answer_correct
                        ]);

                        if ($answer_save->save()) { 
                            $transaction_result_answer = 1;
                        } else {
                            $transaction_result_answer = 0;
                        }
                    }
                }
            }
// Отработка устовий транзакции
            if ($transaction_result_poll == 1 && $transaction_result_question == 1 && $transaction_result_answer == 1) {
                $transaction->commit();
            } else {
                $transaction->rollback();
            }

            return $this->redirect(['view', 'id' => $poll_id]);
        } else {
// Если это входящее обращение - отдать модель Poll и данные по категориям            
            $categories = Category::find()->all();
            return $this->render('create', [
                'model' => $model,
                'categories' => $categories
            ]);
        }
    }

    /**
     * Deletes an existing Poll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*
     * Удаление опроса.
     * Поочерёдное удаление из радных таблиц данных по одному опросу, в зависимости от логики связки ключей
     */
    public function actionDelete($id)
    {
        \Yii::$app->db->createCommand()->delete('poll_result_line', ['poll_id' => $id])->execute();
        \Yii::$app->db->createCommand()->delete('poll_answer', ['poll_id' => $id])->execute();
        \Yii::$app->db->createCommand()->delete('poll_question', ['poll_id' => $id])->execute();
        \Yii::$app->db->createCommand()->delete('poll_member', ['poll_id' => $id])->execute();
        \Yii::$app->db->createCommand()->delete('poll_info', ['poll_id' => $id])->execute();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poll::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * Сохранение данних пришедших из процесса прохождения теста
     * Входящие данные - токен, отправленный участнику теста и сгенерированный из суммы полей базы
     */
    public function actionDo($token)
    {
// Естли есть POST -> Сохранить данные прохождения
        if (Yii::$app->request->post()) {
            $result_array = Yii::$app->request->post();
                foreach ($result_array['question'] as $question => $answer) {
                    foreach ($answer as $value) {
                            $result = new Result();
                            $result->member_id = $result_array['member_id'];
                            $result->poll_id = $result_array['poll_id'];
                            $result->question_id = $question;
                            $result->answer_id = $value;
                            $result->answer_time = date ("Y-m-d h:i:s");
                            $result->save();
                    }
                }
// Запись времени старта процесса прохождения теста пользователем
        Member::updateAll(['starttime' => $result_array['starttime']], ['=', 'member_id', $result->member_id]);
// Сохранение переменных в сессию                
        $session = Yii::$app->session;
        $session['member_id'] = $result->member_id;
        $session['poll_id'] = $result->poll_id;
        $this->redirect(['result']);
        } else {
// Если первое обращение (вход по токену) 
        $member = new Member();
        $members = Member::find()->all();
        foreach ($members as $value) {
            if (md5($value->email . $value->name . $value->poll_link . $value->poll_id) == $token) {
                $member['status'] = $value->status;
                $member['name'] = $value->name;
                $member['email'] = $value->email;
                $member['member_id'] = $value->member_id;
                $poll_id = $member['poll_id'] = $value->poll_id;
            }
        }
// Если пользователь по этому токену еще не проходил тест тогда:
        if ($member['status'] == 0) {
        $poll_info = Poll::find()->where(['poll_id' => $poll_id])->one();
                $poll_value['poll_id'] = $poll_info->poll_id;
                $poll_value['description'] = $poll_info->description;

        $questions = Question::find()->where(['poll_id' => $poll_id])->all();
        $answers = Answer::find()->where(['poll_id' => $poll_id])->all();
        return $this->render('do', [
            'poll' => $poll_value,
            'questions' => $questions,
            'answers' => $answers,
            'member' => $member,
        ]);
        } else {
// Если пользователь по этому токену уже прошёл тест - тогда показываем view - polldone
            return $this->redirect(['polldone']);
        }
    }
    }

    /*
     * Вывод результатов теста пользователю. Если в настройках теста задано их показывать
     */
    
    public function actionResult () {
        $session   = Yii::$app->session;
        $poll_info = Poll::find()->where(['poll_id' => $session['poll_id']])->one();
        $questions = Question::find()->where(['poll_id' => $session['poll_id']])->all();
        $answers   = Answer::find()->where(['poll_id' => $session['poll_id']])->all();
        $results   = Result::find()->where(['poll_id' => $session['poll_id'], 'member_id' => $session['member_id']])->all();

// Запись времени конца процесса прохождения теста пользователем
        Member::updateAll(['duetime' => date("Y-m-d h:i:s")], ['=', 'member_id', $session['member_id']]);
        
        return  $this->render('result',[
            'results' => $results,
            'questions' => $questions,
            'answers' => $answers,
            'poll_info' => $poll_info
        ]);
    }
    
    /*
     * Вывод данных по статистике прохождения пользователями опроса
     */
    public function actionTotal ($id) {
        $members = Member::find()->where(['poll_id' => $id])->all();
        return  $this->render('total',[
            'members' => $members
        ]);
    }
    
    /*
     * Вывод сообщения что тест уже был пройден ранее
     */
    public function actionPolldone() {
        return $this->render('polldone');
    }
    
    /*
     * Сохранение процента прохождения теста в базу. Подвязывается в троку member-a
     */
    public function closeMemberStatus ($persent) {
        $session = Yii::$app->session;
        Member::updateAll(['status' => 1, 'rate' => $persent], ['=', 'member_id', $session['member_id']]);
        self::closePollStatus($session['poll_id']);
        return TRUE;
    }
    /*
     * Если ВСЕ позьзователи прошли тест, изменение статуса самого теста на "Пройден"
     * 
     */
    protected function closePollStatus ($poll_id) {
        $count_done = Member::find()->where(['status' => 0, 'poll_id' => $poll_id])->count();
        if ($count_done==0) {
            Poll::updateAll(['status' => 2], ['=', 'poll_id', $poll_id]);
        }
        return TRUE;
    }
    
    
}