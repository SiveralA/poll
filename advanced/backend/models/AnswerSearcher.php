<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Answer;

/**
 * AnswerSearcher represents the model behind the search form about `app\models\Answer`.
 */
class AnswerSearcher extends Answer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer_id', 'poll_id', 'question_id', 'correct'], 'integer'],
            [['answer_text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Answer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'answer_id' => $this->answer_id,
            'poll_id' => $this->poll_id,
            'question_id' => $this->question_id,
            'correct' => $this->correct,
        ]);

        $query->andFilterWhere(['like', 'answer_text', $this->answer_text]);

        return $dataProvider;
    }
}
