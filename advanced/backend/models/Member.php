<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "poll_member".
 *
 * @property integer $member_id
 * @property string $email
 * @property string $poll_link
 * @property integer $status
 * @property integer $poll_id
 *
 * @property Info $poll
 * @property ResultLine[] $resultLines
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'poll_link', 'poll_id'], 'required'],
            [['status', 'poll_id'], 'integer'],
            [['email'], 'string', 'max' => 64],
            [['poll_link'], 'string', 'max' => 128],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Info::className(), 'targetAttribute' => ['poll_id' => 'poll_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_id' => 'Member ID',
            'email' => 'Email',
            'poll_link' => 'Poll Link',
            'status' => 'Status',
            'poll_id' => 'Poll ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Info::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultLines()
    {
        return $this->hasMany(ResultLine::className(), ['member_id' => 'member_id']);
    }
}
