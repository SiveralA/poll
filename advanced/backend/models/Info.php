<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%info}}".
 *
 * @property integer $poll_id
 * @property integer $owner
 * @property string $date_create
 * @property integer $category_id
 * @property integer $status
 * @property string $description
 * @property string $start_time
 * @property string $finish_time
 * @property integer $visible
 *
 * @property Answer[] $answers
 * @property User $owner0
 * @property Category $category
 * @property Member[] $members
 * @property Question[] $questions
 * @property ResultLine[] $resultLines
 */
class Info extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner', 'category_id'], 'required'],
            [['owner', 'category_id', 'status', 'visible'], 'integer'],
            [['date_create', 'start_time', 'finish_time'], 'safe'],
            [['description'], 'string', 'max' => 255],
            [['owner'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'poll_id' => 'Poll ID',
            'owner' => 'Owner',
            'date_create' => 'Date Create',
            'category_id' => 'Category ID',
            'status' => 'Status',
            'description' => 'Description',
            'start_time' => 'Start Time',
            'finish_time' => 'Finish Time',
            'visible' => 'Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(Member::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['poll_id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultLines()
    {
        return $this->hasMany(ResultLine::className(), ['poll_id' => 'poll_id']);
    }
}
