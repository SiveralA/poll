<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Info;

/**
 * InfoSearcher represents the model behind the search form about `app\models\Info`.
 */
class InfoSearcher extends Info
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poll_id', 'owner', 'category_id', 'status', 'visible'], 'integer'],
            [['date_create', 'description', 'start_time', 'finish_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Info::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'poll_id' => $this->poll_id,
            'owner' => $this->owner,
            'date_create' => $this->date_create,
            'category_id' => $this->category_id,
            'status' => $this->status,
            'start_time' => $this->start_time,
            'finish_time' => $this->finish_time,
            'visible' => $this->visible,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
