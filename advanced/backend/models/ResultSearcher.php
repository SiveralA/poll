<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Result;

/**
 * ResultSearcher represents the model behind the search form about `app\models\Result`.
 */
class ResultSearcher extends Result
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result_id', 'member_id', 'poll_id', 'question_id', 'answer_id'], 'integer'],
            [['answer_time', 'free_variant'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Result::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'result_id' => $this->result_id,
            'member_id' => $this->member_id,
            'poll_id' => $this->poll_id,
            'question_id' => $this->question_id,
            'answer_id' => $this->answer_id,
            'answer_time' => $this->answer_time,
        ]);

        $query->andFilterWhere(['like', 'free_variant', $this->free_variant]);

        return $dataProvider;
    }
}
